import Banner from '../components/Banner.js'
import Highlights from '../components/Highlights.js'
import CourseCard from 	'../components/CourseCard.js'

export default function Home() {
	return(
		 //Fragment "<> and </>" para d mag kagulo
		<>
		<Banner/>
    	<Highlights/>
    	<CourseCard/>
    	
    	</>
		)
}