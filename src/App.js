import './App.css';
// Import AppNavBar
import AppNavbar from './components/AppNavbar.js'
import {Container} from 'react-bootstrap'
import Home from './pages/Home.js'
function App() {
  return (
    //Fragment "<> and </>" para d mag kagulo
    /*We are mounting our component and to prepare for output rendering: self closing*/
    <>
    <AppNavbar/>

    <Container>
        <Home/> 
    </Container>
  
    </>
  );
}

export default App;
