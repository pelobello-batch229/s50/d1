import {Button, Card} from 'react-bootstrap'
export default function CourseCard() {
	return (
			<Card className="cardCourse m-4">
                <Card.Body>
                    <Card.Title>
                        <h4>Sample Course</h4>
                    </Card.Title>
                    <Card.Text>
                        <h5>Description:</h5>
                        <p>This is a sample course offering</p>
                        <h5>Price:</h5>
                        <h5>PHP 40,000</h5>
                    </Card.Text>
                    <Button variants="primary">Enroll</Button>
                </Card.Body>
             </Card>
		)
}